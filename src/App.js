import React from "react";

// Načti z localstorage, pokud není tak načti empty
var users = JSON.parse(localStorage.getItem('items')) || [];
class App extends React.Component {
  constructor(){
    super();
    this.state = {
      items: users
    }

    // Bind funkci
    this.addUser = this.addUser.bind(this);
    this.clearList = this.clearList.bind(this);
  }

  addUser(e) {

    e.preventDefault(); // Ať se nesubmitne
    e.stopPropagation(); // Ať se nesubmitne

    var itemArray = this.state.items; // Načti si současný list do promenne bokem

    itemArray.push( // Pushní tam nové data z inputů
      {
        jmeno: this._inputJmeno.value,
        prijmeni: this._inputPrijmeni.value,
        email: this._inputEmail.value,
        key: Date.now() // Kvůli unikátnímu id, prý to tam má být 
      }
    );

    this.setState({ // Načti to zpět (aktualizuj stav)
      items: itemArray
    });

    // Aktualizuj i ve storage
    localStorage.setItem('items', JSON.stringify(this.state.items));

    // Clearni inputy
    this._inputJmeno.value="";
    this._inputPrijmeni.value="";
    this._inputEmail.value="";

    // Ať se nesubmitne
    return false;
  }

  clearList(e){ // Funkce pro smazání listu
    if(!confirm("Přejete si vymazat seznam uživatelů?")) return false; // Zeptat se

    this.setState({ // Načti prázdné pole
      items: []
    });

    localStorage.clear(); // Vymaž i ve storage
    e.preventDefault();
  }
  render(){
    return (
      <div>
        <div className="row">
          <h4>Zadejte údaje o uživateli</h4>
          <form onSubmit={this.addUser}>
            <div className="form-group">
              
                <label htmlFor="inputJmeno">Jméno</label>
                <input className="form-control" type="text" ref={(a) => this._inputJmeno = a} placeholder="Zadejte jméno" required/>

            </div>
            <div className="form-group">
                <label htmlFor="inputPrijmeni">Příjmení</label>
                <input className="form-control" type="text" ref={(a) => this._inputPrijmeni = a} placeholder="Zadejte příjmení" required/>
            </div>
            <div className="form-group">
                <label htmlFor="inputEmail">Email</label>
                <input className="form-control" type="email" ref={(a) => this._inputEmail = a} placeholder="Zadejte email" required/>  
            </div>         
            <div className="form-group">
              <button className="btn btn-default" type="submit">Přidat uživatele</button>
            </div>  
          </form>
        </div>

        <div className="row">
          <h4>Seznam uživatelů</h4>
          <UserList entries={this.state.items}/>
        </div>

        <div className="row">
          <a onClick={this.clearList} href="#">Vymazat seznam</a>
        </div>
      </div>
      )
    }
}

var UserList = React.createClass({
  render: function() {
    var users = this.props.entries; // Načti z prop parametru

    function displayUsers(item) { // Funkce pro budoucí .map
      return <li className="userItem" key={item.key}><b>{item.jmeno} {item.prijmeni}</b> <br />({item.email})</li>
    }

    var listItems = <span style={{color:"#CCC"}} >Seznam je prázdný</span>; // Defaultně zobrazit že je prázdný

    if (users.length > 0){  // Pokud prázdný není, pak načti seznam
      listItems = users.map(displayUsers);
    }

    // Zobraz user list
    return (
      <ul>
        {listItems}
      </ul>
    );
  }
});


export default App